﻿int[] numbers = { 5, 8, 3, 12, 9 };

// Khởi tạo biến max là phần tử đầu tiên trong mảng
int max = numbers[0];

// Duyệt qua từng phần tử trong mảng để tìm giá trị lớn nhất
for (int i = 1; i < numbers.Length; i++)
{
    // So sánh giá trị hiện tại với giá trị max
    // Nếu giá trị hiện tại lớn hơn max thì gán giá trị hiện tại cho max
    if (numbers[i] > max)
    {
        max = numbers[i];
    }
}

// In ra giá trị lớn nhất
Console.WriteLine("Giá trị lớn nhất trong mảng là: " + max);